<?php

declare(strict_types=1);

namespace Bittacora\Dtos\Tests;

use Bittacora\Dtos\Tests\Fixtures\WithCustomFieldCastDto;
use Bittacora\Dtos\Tests\Fixtures\WithCustomTypeCast;
use Bittacora\Dtos\Tests\Fixtures\WithNullFieldsDto;
use Bittacora\Dtos\Tests\Fixtures\WithOptionalFieldsDto;
use Bittacora\Dtos\Tests\Fixtures\WithPrimitivesDto;
use PHPUnit\Framework\TestCase;

final class DtoTest extends TestCase
{
    public function testCreaUnDtoConCamposPrimitivos(): void
    {
        $dto = WithPrimitivesDto::fromArray([
            'int-parameter' => '1',
            'string_parameter' => 'cadena de texto',
            'float_parameter' => '11.99',
            'bool-parameter' => '1',
        ]);

        self::assertSame(1, $dto->intParameter);
        self::assertSame('cadena de texto', $dto->stringParameter);
        self::assertSame(11.99, $dto->floatParameter);
        self::assertTrue($dto->boolParameter);
    }

    public function testPermiteCamposOpcionales(): void
    {
        $dto = WithOptionalFieldsDto::fromArray([
            'optional_field2' => 'valor establecido'
        ]);

        self::assertSame('valor por defecto', $dto->optionalField);
        self::assertSame('valor establecido', $dto->optionalField2);
    }

    public function testAdmiteNull(): void
    {
        $dto = WithNullFieldsDto::fromArray(['nullable' => null]);
        self::assertNull($dto->nullable);
    }

    public function testSePuedenUsarCastsPersonalizadosPorTipo(): void
    {
        $dto = WithCustomTypeCast::fromArray([
            'numeric-string' => 1,
            'customTypeParameter' => '2',
        ]);
        self::assertSame('Número 1', $dto->numericString);
        self::assertSame('El valor del parámetro fue 2', $dto->customTypeParameter->exampleMethod());
    }

    public function testSePuedenUsarCastsPersonalizadosPorCampo(): void
    {
        $dto = WithCustomFieldCastDto::fromArray(['custom-field' => 'probando']);
        self::assertSame('El valor del parámetro fue probando', $dto->customField->exampleMethod());
    }
}
