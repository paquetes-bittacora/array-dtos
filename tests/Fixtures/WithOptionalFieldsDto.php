<?php

declare(strict_types=1);

namespace Bittacora\Dtos\Tests\Fixtures;

use Bittacora\Dtos\Dto;

final class WithOptionalFieldsDto extends Dto
{
    public function __construct(
        public readonly string $optionalField = 'valor por defecto',
        public readonly string $optionalField2 = '',
    ) {
    }
}
