<?php

declare(strict_types=1);

namespace Bittacora\Dtos\Tests\Fixtures;

final class CustomType
{
    public function __construct(
        public readonly string $parameter
    ) {
    }

    public function exampleMethod(): string
    {
        return 'El valor del parámetro fue ' . $this->parameter;
    }
}
