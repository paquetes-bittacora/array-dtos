<?php

declare(strict_types=1);

namespace Bittacora\Dtos\Tests\Fixtures;

use Bittacora\Dtos\Dto;

final class WithCustomTypeCast extends Dto
{
    public function __construct(
        public readonly string $numericString,
        public readonly CustomType $customTypeParameter,
    ) {
    }

    protected static function getCustomTypeCast(): array
    {
        return [
            'string' => self::customStringCast(...),
            CustomType::class => self::castToCustomType(...),
        ];
    }

    private static function customStringCast(string $field, string $type, mixed $value, bool $allowsNull): string
    {
        return "Número $value";
    }

    protected static function castToCustomType(string $field, string $type, mixed $value, bool $allowsNull): CustomType
    {
        return new CustomType($value);
    }
}
