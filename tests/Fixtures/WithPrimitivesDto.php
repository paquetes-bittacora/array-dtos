<?php

declare(strict_types=1);

namespace Bittacora\Dtos\Tests\Fixtures;

use Bittacora\Dtos\Dto;

final class WithPrimitivesDto extends Dto
{
    public function __construct(
        public readonly int $intParameter,
        public readonly string $stringParameter,
        public readonly float $floatParameter,
        public readonly bool $boolParameter,
    ) {
    }
}
