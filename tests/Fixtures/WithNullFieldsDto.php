<?php

declare(strict_types=1);

namespace Bittacora\Dtos\Tests\Fixtures;

use Bittacora\Dtos\Dto;

final class WithNullFieldsDto extends Dto
{
    public function __construct(
        public ?int $nullable = null,
    ) {
    }
}
