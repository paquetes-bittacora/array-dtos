<?php

declare(strict_types=1);

namespace Bittacora\Dtos\Tests\Fixtures;

use Bittacora\Dtos\Dto;

final class WithCustomFieldCastDto extends Dto
{
    public function __construct(
        public readonly CustomType $customField,
    )
    {
    }

    protected static function getCustomFieldCasts(): array
    {
        return [
            'customField' => self::castToCustomType(...)
        ];
    }

    protected static function castToCustomType(string $field, string $type, mixed $value, bool $allowsNull): CustomType
    {
        return new CustomType($value);
    }
}
