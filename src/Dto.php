<?php

namespace Bittacora\Dtos;

use ReflectionClass;

abstract class Dto
{
    public static function fromArray(array $data): static {
        $camelCasedData = self::getCamelCasedData($data);
        /** @phpstan-ignore-next-line */
        return new static(...self::getConstructorArguments($camelCasedData));
    }

    /**
     * Debe devolver un array en el que las claves sean los tipos sobre los que se quiere hacer un cast personalizado,
     * y los valores callables a los que se le pasarán los mismos valores que a la función cast de esta misma clase.
     * @return array<string, callable>|array
     */
    protected static function getCustomTypeCast(): array
    {
        return [];
    }

    /**
     * Debe devolver un array en el que las claves sean los campos del DTO sobre los que se quiere hacer un cast
     * personalizado, y los valores callables a los que se le pasarán los mismos valores que a la función cast de esta
     * misma clase.
     * @return array<string, callable>|array
     */
    protected static function getCustomFieldCasts(): array
    {
        return [];
    }

    private static function toCamelCase(string $string): string
    {
        return lcfirst(str_replace(['-', '_'], '', ucwords($string, '-_')));
    }

    /**
     * @template T
     * @param string $field
     * @param class-string<T>|key-of<static::CUSTOM_TYPE_CASTS> $type
     * @param mixed $value
     * @param bool $allowsNull
     * @return T
     */
    private static function cast(string $field, string $type, mixed $value, bool $allowsNull)
    {
        // Casts personalizados por campo, definidos por configuración
        $customFieldCasts = static::getCustomFieldCasts();
        if (isset($customFieldCasts[$field])) {
            return $customFieldCasts[$field](...func_get_args());
        }

        // Casts personalizados por tipo definidos por configuración
        $customTypeCasts = static::getCustomTypeCast();
        if (isset($customTypeCasts[$type])) {
            return $customTypeCasts[$type](...func_get_args());
        }

        if ($allowsNull && null === $value) {
            return null;
        }

        $castMethodName = 'cast' . ucfirst($type);
        return match ($type) {
            'integer', 'int' => (int) $value,
            'boolean', 'bool' => (bool) $value,
            'array' => (array) $value,
            'float' => (float) $value,
            'string' => (string) $value,
            default => static::tryCustomCast($castMethodName, $value),
        };
    }

    private static function getCamelCasedData(array $data): array
    {
        $camelCasedData = [];
        foreach ($data as $k => $v) {
            $camelCasedData[self::toCamelCase($k)] = $v;
        }
        return $camelCasedData;
    }

    private static function getConstructorArguments(array $camelCasedData): array
    {
        $constructorArguments = [];
        $reflection = new ReflectionClass(static::class);
        foreach ($reflection->getConstructor()?->getParameters() as $parameter) {
            $parameterName = $parameter->getName();
            if (array_key_exists($parameterName, $camelCasedData)) {
                $constructorArguments[$parameterName] = self::cast(
                    $parameterName,
                    $parameter->getType()?->getName(),
                    $camelCasedData[$parameterName],
                    $parameter->allowsNull(),
                );
            }
        }
        return $constructorArguments;
    }

    private static function tryCustomCast(string $castMethodName, mixed $value)
    {
        if (!method_exists(static::class, $castMethodName)) {
            return $value;
        }
        return static::$castMethodName($value);
    }
}
